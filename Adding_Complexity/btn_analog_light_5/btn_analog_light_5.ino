// An example of a toggle button with "is pressed" state
// enabling an LED with adjustable brightness.
// uses: button, LED, potentiometer.


int pin_btn = 13;
int pin_led =  3;
int pin_dial = A1;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin_btn, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_dial, INPUT);
  Serial.begin(115200);
}


// Toggle button with pressed state
bool toggle = 0; // toggle value
bool btn_hold = 0; // is button pressed status
bool check_btn(){
  if (digitalRead(pin_btn) == 0) { // FALSE is pressed, inverted voltage logic
    if(btn_hold == 0){ // only invert toggle once if the button is held down
      toggle = !toggle; // This sets toggle to the value of NOT toggle i.e. inverting 1 and 0.
    }
    btn_hold = 1; // the button is currently being held down
    return btn_hold;
  }
  return btn_hold = 0; // set hold to zero and then return it.
}

// all the values, conversions and output kept together to read,
int get_brightness(){ // return an integer from this function, rather than nothing (void)
  int dial = analogRead(pin_dial);
  int scaled = map(dial, 1,1024,0,255);
  return scaled;
}

void loop() {
  // put your main code here, to run repeatedly:
  bool pressed = check_btn(); // update the button state variables
  if(pressed){
    Serial.println("button is pressed");
  } else {
    Serial.println("button not pressed");
  }

  if(toggle) {
    Serial.print("button toggled on." );

    int amount = set_brightness();
    Serial.print("Brightness: " );
    Serial.print(amount);
  } else {
    Serial.print("button toggle off");
    analogWrite(pin_led, 0);
  }

  Serial.println(); // Always use .print() above this will end the line when the program loops.
  delay(300);
}
// There are some bugs in this; it only works right if you use the button slowly.
// pressing the button quicker than 300ms appart overruns trying to guess if it's held down.
