# Example about adding complecity to a program
The folder `btn_analog_light` contains the final version. Each `*_<number>` folder hold itterative versions.

There is a [presentation here](https://docs.google.com/presentation/d/e/2PACX-1vRwSPoGvApXlrf_6gjtKXy4ZxFLx0Or-atugtrIFevc1lEZIzZaCy1PVJdkP8BoOnsKJbsRnAnAFcbA/pub?start=false&loop=false&delayms=30000) to go along with these files, although the git commits and code comments should explain a lot.

You can also view the git history of the final version to see the changes made along with comments.

each commit is going to  change the final version and also include what's changes as a numbered file, this is so you aren't requred to know git to access the revisions.

## History
Overall history view of the porject can is here: https://gitlab.com/BirkdaleHigh/arduino-examples/commits/master/Adding_Complexity

The list below are some key changes.

1. [Starting file](https://gitlab.com/BirkdaleHigh/arduino-examples/blob/eabd7b3f5e3d9c96ab8280ea516d50ef6bb7d123/Adding_Complexity/btn_analog_light/btn_analog_light.ino)
1. [New button function](https://gitlab.com/BirkdaleHigh/arduino-examples/commit/3b51fb15fdd6a5647503df4c2b3759f1e89156bb)
1. [New brightness function](https://gitlab.com/BirkdaleHigh/arduino-examples/commit/ed99a5603805e110868521f601ef58bf48687faf)
1. [Adding code comments and a function return value](https://gitlab.com/BirkdaleHigh/arduino-examples/commit/2749ff69be1e077e8356d6a6b59a086f0832565a)
1. [Add another return value and move serial output](https://gitlab.com/BirkdaleHigh/arduino-examples/commit/f068e0da539d8c68b8cad3d1384f2bf07a8b2a4b)