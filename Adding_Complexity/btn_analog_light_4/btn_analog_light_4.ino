// An example of a toggle button with "is pressed" state
// enabling an LED with adjustable brightness.
// uses: button, LED, potentiometer.


int pin_btn = 13;
int pin_led =  3;
int pin_dial = A1;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin_btn, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_dial, INPUT);
  Serial.begin(115200);
}

// Toggle button with pressed state
bool toggle = 0; // toggle state value
bool btn_hold = 0; // is button currently pressed state
void check_btn(){
  if (digitalRead(pin_btn) == 1) { // true means pressed
    if(btn_hold == 0){ // only invert toggle once if the button is held down
      toggle = !toggle; // This sets toggle to the value of NOT toggle i.e. inverting 1 and 0.
    }
    btn_hold = 1;
    Serial.println("button pressed");
  } else {
    btn_hold = 0;
    Serial.println("button not pressed");
  }
}

// all the values, conversions and output kept together
int set_brightness(){ // return an integer from this function, rather than nothing (void)
  int dial = analogRead(pin_dial);
  int amount = map(dial, 0,1024,0,255);
  analogWrite(pin_led, amount);
  return amount;
}

void loop() {
  // put your main code here, to run repeatedly:
  check_btn();

  if(toggle) {
    Serial.print("button toggled on." );

    int amount = set_brightness();
    Serial.print("Brightness: " );
    Serial.println(amount);
  } else {
    analogWrite(pin_led, 0);
    Serial.println("toggle off");
  }

  delay(300);
}
